package com.da.practicaltask.moviecataloguebackend.controller;

import com.da.practicaltask.moviecataloguebackend.exception.MovieNotFoundException;
import com.da.practicaltask.moviecataloguebackend.model.MovieDto;
import com.da.practicaltask.moviecataloguebackend.model.MovieRequest;
import com.da.practicaltask.moviecataloguebackend.service.MovieService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(MovieController.class)
class MovieControllerTest {

    private static final long ID = 1L;
    private static final String TITLE = "title1";
    private static final int DURATION = 120;
    private static final BigDecimal PRICE = BigDecimal.valueOf(100);

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private MovieService movieService;

    private MovieDto movieDto;
    private MovieRequest movieRequest;

    @BeforeEach
    void setUp() {
        movieDto = MovieDto.builder()
                .id(ID)
                .title(TITLE)
                .duration(DURATION)
                .price(PRICE)
                .build();

        movieRequest = MovieRequest.builder()
                .title(TITLE)
                .duration(DURATION)
                .price(PRICE)
                .build();
    }

    /**
     * @verifies return ResponseEntity with the status code 200 and the found MovieDto in the body
     * @see MovieController#get(long)
     */
    @Test
    public void get_shouldReturnResponseEntityWithTheStatusCode200AndTheFoundMovieDtoInTheBody() throws Exception {
        when(movieService.findById(ID)).thenReturn(movieDto);

        mockMvc.perform(get("/movies/{id}", ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(ID))
                .andExpect(jsonPath("$.title").value(TITLE))
                .andExpect(jsonPath("$.duration").value(DURATION))
                .andExpect(jsonPath("$.price").value(PRICE));

        verify(movieService, times(1)).findById(ID);
    }

    /**
     * @verifies throw MovieNotFoundException if the specified Movie id doesn't exist
     * @see MovieController#get(long)
     */
    @Test
    public void get_shouldThrowMovieNotFoundExceptionIfTheSpecifiedMovieIdDoesntExist() throws Exception {
        when(movieService.findById(ID)).thenThrow(MovieNotFoundException.class);

        mockMvc.perform(get("/movies/{id}", ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

        verify(movieService, times(1)).findById(ID);
    }

    /**
     * @verifies return ResponseEntity with the status code 200 and the saved MovieDto in the body
     * @see MovieController#create(com.da.practicaltask.moviecataloguebackend.model.MovieRequest)
     */
    @Test
    public void create_shouldReturnResponseEntityWithTheStatusCode200AndTheSavedMovieDtoInTheBody() throws Exception {
        when(movieService.save(movieRequest)).thenReturn(movieDto);

        mockMvc.perform(post("/movies")
                .content(objectMapper.writeValueAsString(movieRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(ID))
                .andExpect(jsonPath("$.title").value(TITLE))
                .andExpect(jsonPath("$.duration").value(DURATION))
                .andExpect(jsonPath("$.price").value(PRICE));

        verify(movieService, times(1)).save(movieRequest);
    }

    /**
     * @verifies return ResponseEntity with the status code 204 if Movie was deleted
     * @see MovieController#delete(long)
     */
    @Test
    public void delete_shouldReturnResponseEntityWithTheStatusCode204IfMovieWasDeleted() throws Exception {
        doNothing().when(movieService).deleteById(ID);

        mockMvc.perform(delete("/movies/{id}", ID))
                .andDo(print())
                .andExpect(status().isNoContent());

        verify(movieService, times(1)).deleteById(ID);
    }

    /**
     * @verifies throw MovieNotFoundException if the specified Movie id doesn't exist
     * @see MovieController#delete(long)
     */
    @Test
    public void delete_shouldThrowMovieNotFoundExceptionIfTheSpecifiedMovieIdDoesntExist() throws Exception {
        doThrow(MovieNotFoundException.class).when(movieService).deleteById(ID);

        mockMvc.perform(delete("/movies/{id}", ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

        verify(movieService, times(1)).deleteById(ID);
    }

    /**
     * @verifies return ResponseEntity with the status code 200 and the created or updated MovieDto in the body
     * @see MovieController#update(long, MovieRequest)
     */
    @Test
    public void update_shouldReturnResponseEntityWithTheStatusCode200AndTheCreatedOrUpdatedMovieDtoInTheBody() throws Exception {
        when(movieService.update(ID, movieRequest)).thenReturn(movieDto);

        mockMvc.perform(put("/movies/{id}", ID)
                .content(objectMapper.writeValueAsString(movieRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(ID))
                .andExpect(jsonPath("$.title").value(TITLE))
                .andExpect(jsonPath("$.duration").value(DURATION))
                .andExpect(jsonPath("$.price").value(PRICE));

        verify(movieService, times(1)).update(ID, movieRequest);
    }

    /**
     * @verifies throw MovieNotFoundException if the specified Movie id doesn't exist
     * @see MovieController#update(long, MovieRequest)
     */
    @Test
    public void update_shouldThrowMovieNotFoundExceptionIfTheSpecifiedMovieIdDoesntExist() throws Exception {
        when(movieService.update(ID, movieRequest)).thenThrow(MovieNotFoundException.class);

        mockMvc.perform(put("/movies/{id}", ID)
                .content(objectMapper.writeValueAsString(movieRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

        verify(movieService, times(1)).update(ID, movieRequest);
    }
}