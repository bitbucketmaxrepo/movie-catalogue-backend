package com.da.practicaltask.moviecataloguebackend.controller;

import com.da.practicaltask.moviecataloguebackend.dao.MovieRepository;
import com.da.practicaltask.moviecataloguebackend.dao.entity.Movie;
import com.da.practicaltask.moviecataloguebackend.mapper.MovieMapper;
import com.da.practicaltask.moviecataloguebackend.model.MovieDto;
import com.da.practicaltask.moviecataloguebackend.model.MovieRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.HttpStatus.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration-test")
public class MovieControllerIntegrationTest {

    private static final long NOT_EXISTING_ID = 100L;
    private static final String TITLE = "title1";
    private static final String UPDATED_TITLE = "title2";
    private static final int DURATION = 120;
    private static final int UPDATED_DURATION = 180;
    private static final BigDecimal PRICE = new BigDecimal("100.00");
    private static final BigDecimal UPDATED_PRICE = new BigDecimal("200.00");

    @LocalServerPort
    private int port;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieMapper movieMapper;

    @Autowired
    private TestRestTemplate restTemplate;

    private Movie movie;
    private MovieRequest movieRequest;

    @BeforeEach
    void setUp() {
        movie = new Movie();
        movie.setTitle(TITLE);
        movie.setDuration(DURATION);
        movie.setPrice(PRICE);

        movieRequest = MovieRequest.builder()
                .title(UPDATED_TITLE)
                .duration(UPDATED_DURATION)
                .price(UPDATED_PRICE)
                .build();
    }

    @Test
    public void get_returnMovieIfTheSpecifiedMovieIdExists() {
        Movie expectedMovie = movieRepository.save(movie);

        ResponseEntity<MovieDto> actualResponseEntity =
                restTemplate.getForEntity("http://localhost:" + port + "/movies/" + expectedMovie.getId(), MovieDto.class);

        assertEquals(OK, actualResponseEntity.getStatusCode());
        assertEquals(movieMapper.movieToMovieDto(expectedMovie), actualResponseEntity.getBody());
    }

    @Test
    public void get_returnTheStatusCode404IfTheSpecifiedMovieIdDoesntExist() {
        assertFalse(movieRepository.findById(NOT_EXISTING_ID).isPresent());

        ResponseEntity<String> actualResponseEntity =
                restTemplate.getForEntity("http://localhost:" + port + "/movies/" + NOT_EXISTING_ID, String.class);

        assertEquals(NOT_FOUND, actualResponseEntity.getStatusCode());
    }

    @Test
    public void create_returnTheSavedMovie() {
        ResponseEntity<MovieDto> actualResponseEntity =
                restTemplate.postForEntity("http://localhost:" + port + "/movies", movieRequest, MovieDto.class);

        MovieDto expectedMovieDto =
                movieMapper.movieToMovieDto(movieRepository.findById(actualResponseEntity.getBody().getId()).get());

        assertEquals(OK, actualResponseEntity.getStatusCode());
        assertEquals(expectedMovieDto, actualResponseEntity.getBody());
    }

    @Test
    public void delete_deleteMovieIfTheSpecifiedMovieIdExists() {
        long existingMovieId = movieRepository.save(movie).getId();

        ResponseEntity<String> actualResponseEntity = restTemplate.exchange(
                "http://localhost:" + port + "/movies/" + existingMovieId,
                HttpMethod.DELETE,
                HttpEntity.EMPTY,
                String.class);

        assertFalse(movieRepository.findById(existingMovieId).isPresent());
        assertEquals(NO_CONTENT, actualResponseEntity.getStatusCode());
    }

    @Test
    public void delete_returnTheStatusCode404IfTheSpecifiedMovieIdDoesntExist() {
        assertFalse(movieRepository.findById(NOT_EXISTING_ID).isPresent());

        ResponseEntity<String> actualResponseEntity = restTemplate.exchange(
                "http://localhost:" + port + "/movies/" + NOT_EXISTING_ID,
                HttpMethod.DELETE,
                HttpEntity.EMPTY,
                String.class);

        assertEquals(NOT_FOUND, actualResponseEntity.getStatusCode());
    }

    @Test
    public void update_updateExistingMovieIfTheSpecifiedMovieIdExists() {
        long existingMovieId = movieRepository.save(movie).getId();

        ResponseEntity<MovieDto> actualResponseEntity =
                restTemplate.exchange(
                        "http://localhost:" + port + "/movies/" + existingMovieId,
                        HttpMethod.PUT,
                        new HttpEntity<>(movieRequest),
                        MovieDto.class);
        MovieDto actualMovieDto = actualResponseEntity.getBody();

        MovieDto expectedMovieDto =
                movieMapper.movieToMovieDto(movieRepository.findById(existingMovieId).get());

        assertEquals(OK, actualResponseEntity.getStatusCode());
        assertEquals(expectedMovieDto, actualMovieDto);
        assertEquals(movieRequest.getTitle(), actualMovieDto.getTitle());
        assertEquals(movieRequest.getDuration(), actualMovieDto.getDuration());
        assertEquals(movieRequest.getPrice(), actualMovieDto.getPrice());
    }

    @Test
    public void update_returnTheStatusCode404IfTheSpecifiedMovieIdDoesntExist() {
        assertFalse(movieRepository.findById(NOT_EXISTING_ID).isPresent());

        ResponseEntity<String> actualResponseEntity = restTemplate.exchange(
                "http://localhost:" + port + "/movies/" + NOT_EXISTING_ID,
                HttpMethod.PUT,
                new HttpEntity<>(movieRequest),
                String.class);

        assertEquals(NOT_FOUND, actualResponseEntity.getStatusCode());
    }
}
