package com.da.practicaltask.moviecataloguebackend.service;

import com.da.practicaltask.moviecataloguebackend.dao.MovieRepository;
import com.da.practicaltask.moviecataloguebackend.dao.entity.Movie;
import com.da.practicaltask.moviecataloguebackend.exception.MovieNotFoundException;
import com.da.practicaltask.moviecataloguebackend.mapper.MovieMapper;
import com.da.practicaltask.moviecataloguebackend.model.MovieDto;
import com.da.practicaltask.moviecataloguebackend.model.MovieRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MovieServiceTest {

    private static final long ID = 1L;
    private static final String TITLE = "title1";
    private static final int DURATION = 120;
    private static final BigDecimal PRICE = BigDecimal.valueOf(100);

    @Mock
    private MovieRepository movieRepository;

    @Mock
    private MovieMapper movieMapper;

    @InjectMocks
    private MovieService movieService;

    private Movie movie;
    private Movie movieWithEmptyId;
    private MovieDto movieDto;
    private MovieRequest movieRequest;

    @BeforeEach
    void setUp() {
        movie = new Movie();
        movie.setId(ID);
        movie.setTitle(TITLE);
        movie.setDuration(DURATION);
        movie.setPrice(PRICE);

        movieWithEmptyId = new Movie();
        movieWithEmptyId.setTitle(TITLE);
        movieWithEmptyId.setDuration(DURATION);
        movieWithEmptyId.setPrice(PRICE);

        movieDto = MovieDto.builder()
                .id(ID)
                .title(TITLE)
                .duration(DURATION)
                .price(PRICE)
                .build();

        movieRequest = MovieRequest.builder()
                .title(TITLE)
                .duration(DURATION)
                .price(PRICE)
                .build();
    }

    /**
     * @verifies return MovieDto if the specified Movie id exists
     * @see MovieService#findById(long)
     */
    @Test
    public void findById_shouldReturnMovieDtoIfTheSpecifiedMovieIdExists() throws Exception {
        when(movieRepository.findById(ID)).thenReturn(Optional.of(movie));
        when(movieMapper.movieToMovieDto(movie)).thenReturn(movieDto);

        MovieDto actualMovieDto = movieService.findById(ID);

        verify(movieRepository, times(1)).findById(ID);
        verify(movieMapper, times(1)).movieToMovieDto(movie);
        assertEquals(movieDto, actualMovieDto);
    }

    /**
     * @verifies throw MovieNotFoundException if the specified Movie id doesn't exist
     * @see MovieService#findById(long)
     */
    @Test
    public void findById_shouldThrowMovieNotFoundExceptionIfTheSpecifiedMovieIdDoesntExist() {
        when(movieRepository.findById(ID)).thenReturn(Optional.empty());

        assertThrows(MovieNotFoundException.class, () -> movieService.findById(ID));
        verify(movieRepository, times(1)).findById(ID);
    }

    /**
     * @verifies return MovieDto for the saved movie
     * @see MovieService#save(MovieRequest)
     */
    @Test
    void save_shouldReturnMovieDtoForTheSavedMovie() {
        when(movieMapper.movieRequestToMovie(movieRequest)).thenReturn(movieWithEmptyId);
        when(movieRepository.save(movieWithEmptyId)).thenReturn(movie);
        when(movieMapper.movieToMovieDto(movie)).thenReturn(movieDto);

        MovieDto actualMovieDto = movieService.save(movieRequest);

        verify(movieMapper, times(1)).movieRequestToMovie(movieRequest);
        verify(movieRepository, times(1)).save(movieWithEmptyId);
        verify(movieMapper, times(1)).movieToMovieDto(movie);
        assertEquals(movieDto, actualMovieDto);
    }

    /**
     * @verifies delete Movie with the specified id
     * @see MovieService#deleteById(long)
     */
    @Test
    void deleteById_shouldDeleteMovieWithTheSpecifiedId() throws MovieNotFoundException {
        doNothing().when(movieRepository).deleteById(ID);

        movieService.deleteById(ID);

        verify(movieRepository, times(1)).deleteById(ID);
    }

    /**
     * @verifies throw MovieNotFoundException if the specified Movie id doesn't exist
     * @see MovieService#deleteById(long)
     */
    @Test
    void deleteById_shouldThrowMovieNotFoundExceptionIfTheSpecifiedMovieIdDoesntExist() {
        doThrow(EmptyResultDataAccessException.class).when(movieRepository).deleteById(ID);

        assertThrows(MovieNotFoundException.class, () -> movieService.deleteById(ID));
        verify(movieRepository, times(1)).deleteById(ID);
    }

    /**
     * @verifies return MovieDto for the created or updated movie
     * @see MovieService#update(long, MovieRequest)
     */
    @Test
    void update_shouldReturnMovieDtoForTheCreatedOrUpdatedMovie() throws Exception {
        when(movieRepository.findById(ID)).thenReturn(Optional.of(movie));
        when(movieMapper.movieRequestToMovie(movieRequest)).thenReturn(movieWithEmptyId);
        when(movieRepository.save(movie)).thenReturn(movie);
        when(movieMapper.movieToMovieDto(movie)).thenReturn(movieDto);

        MovieDto actualMovieDto = movieService.update(ID, movieRequest);

        verify(movieMapper, times(1)).movieRequestToMovie(movieRequest);
        verify(movieRepository, times(1)).save(movie);
        verify(movieMapper, times(1)).movieToMovieDto(movie);
        assertEquals(movieDto, actualMovieDto);
    }

    /**
     * @verifies throw MovieNotFoundException if the specified Movie id doesn't exist
     * @see MovieService#update(long, MovieRequest)
     */
    @Test
    public void update_shouldThrowMovieNotFoundExceptionIfTheSpecifiedMovieIdDoesntExist() {
        when(movieRepository.findById(ID)).thenReturn(Optional.empty());

        assertThrows(MovieNotFoundException.class, () -> movieService.update(ID, movieRequest));
        verify(movieRepository, times(1)).findById(ID);
    }
}