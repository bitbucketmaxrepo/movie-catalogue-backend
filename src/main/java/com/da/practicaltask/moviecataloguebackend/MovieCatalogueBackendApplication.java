package com.da.practicaltask.moviecataloguebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieCatalogueBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieCatalogueBackendApplication.class, args);
	}
}
