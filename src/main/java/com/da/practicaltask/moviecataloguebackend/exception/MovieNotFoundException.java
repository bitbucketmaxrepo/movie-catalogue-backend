package com.da.practicaltask.moviecataloguebackend.exception;

public class MovieNotFoundException extends Exception {

    public MovieNotFoundException(long id) {
        super("Movie was not found for id: " + id);
    }
}
