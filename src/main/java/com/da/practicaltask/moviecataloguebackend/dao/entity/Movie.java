package com.da.practicaltask.moviecataloguebackend.dao.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "movie")
@Data
public class Movie {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "duration", nullable = false)
    private int duration;

    @Column(name = "country")
    private String country;

    @Column(name = "language")
    private String language;

    @Column(name = "price", nullable = false)
    private BigDecimal price;
}
