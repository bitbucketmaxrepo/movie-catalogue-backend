package com.da.practicaltask.moviecataloguebackend.dao;

import com.da.practicaltask.moviecataloguebackend.dao.entity.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie, Long> {
}
