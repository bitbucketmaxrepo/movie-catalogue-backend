package com.da.practicaltask.moviecataloguebackend.controller;

import com.da.practicaltask.moviecataloguebackend.exception.MovieNotFoundException;
import com.da.practicaltask.moviecataloguebackend.model.MovieDto;
import com.da.practicaltask.moviecataloguebackend.model.MovieRequest;
import com.da.practicaltask.moviecataloguebackend.service.MovieService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/movies")
@RequiredArgsConstructor
@Slf4j
public class MovieController {

    private final MovieService movieService;

    /**
     * @should return ResponseEntity with the status code 200 and the found MovieDto in the body
     * @should throw MovieNotFoundException if the specified Movie id doesn't exist
     */
    @GetMapping("/{id}")
    public ResponseEntity<MovieDto> get(@PathVariable("id") long id) throws MovieNotFoundException {
        log.info("Getting Movie by id: {}", id);
        return ResponseEntity.ok(movieService.findById(id));
    }

    /**
     * @should return ResponseEntity with the status code 200 and the saved MovieDto in the body
     */
    @PostMapping
    public ResponseEntity<MovieDto> create(@RequestBody MovieRequest movieRequest) {
        log.info("New Movie saving {}", movieRequest);
        return ResponseEntity.ok(movieService.save(movieRequest));
    }

    /**
     * @should return ResponseEntity with the status code 204 if Movie was deleted
     * @should throw MovieNotFoundException if the specified Movie id doesn't exist
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) throws MovieNotFoundException {
        log.info("Deleting Movie by id: {}", id);
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    /**
     * @should return ResponseEntity with the status code 200 and the created or updated MovieDto in the body
     * @should throw MovieNotFoundException if the specified Movie id doesn't exist
     */
    @PutMapping("/{id}")
    public ResponseEntity<MovieDto> update(@PathVariable("id") long id, @RequestBody MovieRequest movieRequest) throws MovieNotFoundException {
        log.info("Updating Movie by id: {}", id);
        return ResponseEntity.ok(movieService.update(id, movieRequest));
    }
}
