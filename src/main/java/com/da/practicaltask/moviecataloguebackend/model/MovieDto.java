package com.da.practicaltask.moviecataloguebackend.model;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class MovieDto {

    private long id;

    private String title;

    private String description;

    private int duration;

    private String country;

    private String language;

    private BigDecimal price;
}
