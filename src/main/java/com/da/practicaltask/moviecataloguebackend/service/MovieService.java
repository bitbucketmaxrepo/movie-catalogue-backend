package com.da.practicaltask.moviecataloguebackend.service;

import com.da.practicaltask.moviecataloguebackend.dao.MovieRepository;
import com.da.practicaltask.moviecataloguebackend.dao.entity.Movie;
import com.da.practicaltask.moviecataloguebackend.exception.MovieNotFoundException;
import com.da.practicaltask.moviecataloguebackend.mapper.MovieMapper;
import com.da.practicaltask.moviecataloguebackend.model.MovieDto;
import com.da.practicaltask.moviecataloguebackend.model.MovieRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MovieService {

    private final MovieRepository movieRepository;

    private final MovieMapper movieMapper;

    /**
     * @should return MovieDto if the specified Movie id exists
     * @should throw MovieNotFoundException if the specified Movie id doesn't exist
     */
    public MovieDto findById(long id) throws MovieNotFoundException {
        Movie movie = movieRepository.findById(id)
                .orElseThrow(() -> new MovieNotFoundException(id));
        return movieMapper.movieToMovieDto(movie);
    }

    /**
     * @should return MovieDto for the saved movie
     */
    public MovieDto save(MovieRequest movieRequest) {
        Movie movie = movieMapper.movieRequestToMovie(movieRequest);
        return movieMapper.movieToMovieDto(movieRepository.save(movie));
    }

    /**
     * @should delete Movie with the specified id
     * @should throw MovieNotFoundException if the specified Movie id doesn't exist
     */
    public void deleteById(long id) throws MovieNotFoundException {
        try {
            movieRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new MovieNotFoundException(id);
        }
    }

    /**
     * @should return MovieDto for the created or updated Movie
     * @should throw MovieNotFoundException if the specified Movie id doesn't exist
     */
    public MovieDto update(long id, MovieRequest movieRequest) throws MovieNotFoundException {
        movieRepository.findById(id)
                .orElseThrow(() -> new MovieNotFoundException(id));
        Movie movie = movieMapper.movieRequestToMovie(movieRequest);
        movie.setId(id);
        return movieMapper.movieToMovieDto(movieRepository.save(movie));
    }
}
