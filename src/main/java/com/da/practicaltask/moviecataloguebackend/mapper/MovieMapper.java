package com.da.practicaltask.moviecataloguebackend.mapper;

import com.da.practicaltask.moviecataloguebackend.dao.entity.Movie;
import com.da.practicaltask.moviecataloguebackend.model.MovieDto;
import com.da.practicaltask.moviecataloguebackend.model.MovieRequest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MovieMapper {

    MovieDto movieToMovieDto(Movie movie);

    Movie movieRequestToMovie(MovieRequest movieRequest);
}
