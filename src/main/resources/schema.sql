DROP TABLE IF EXISTS movie;

CREATE TABLE movie
(
    id BIGINT auto_increment
        primary key,
    title VARCHAR(255) not null,
    description VARCHAR(255),
    duration INTEGER,
    country VARCHAR(255),
    language VARCHAR(255),
    price DECIMAL(20,2)
);