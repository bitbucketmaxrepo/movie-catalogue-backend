##Run
To package the project (create JAR), deploy it and run use:
- Windows - `run.bat`
- Unix - `run.sh`

##Test
REST API service can be tested using Swagger UI:
`http://localhost:8080/swagger-ui.html`