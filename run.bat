@echo off

echo Building...
call mvnw.cmd clean package

echo Running...
java -jar target/movie-catalogue-backend-0.0.1-SNAPSHOT.jar